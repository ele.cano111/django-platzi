
from django.urls import path
from my_app import views as local_views
from posts import views as posts_views
urlpatterns = [
    path('hello-world/',local_views.hello_world),
    path('sorted/', local_views.sort_integers),

    path('posts/', posts_views.list_posts),
]
